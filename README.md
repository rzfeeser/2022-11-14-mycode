# README.md

### About
The purpose of this repository is to experiment with using Markdown inside of GitLab.
The first thing worth noting is that we can type text like we normally would.
Although, there are a few things you might find odd.

By learning a few Markdown tricks, you can get your text to appear "just right".

### Links
Why don't we start by learning
how to create some links within our document

[Links are Easy to Create](https://example.org)
[Alta3 Research](https://alta3.com)

Sometimes we want to use images to create links too!
[![Alta3 Research with Logo](https://labs.alta3.com/courses/gitlab/images/gitlabmarkdown/alta3logo.png)](https://alta3.com)

Sometimes we want to scale down those images (and image links) too!

[<img src="https://labs.alta3.com/courses/gitlab/images/gitlabmarkdown/alta3logo.png" width="20" height="20">](https://alta3.com)

If you are adding an email address to a page be sure to format your link with a `mailto` to avoid broken links. For example,
Author: RZFeeser
Email: [rzfeeser@alta3.com](mailto:rzfeeser@alta3.com)


### Lists

- Lists are easy to create
- Just make hyphens
- And they will be converted to an HTML bulleted-list

Shopping List:
- Farm Supplies
  - 4 hay bales
  - 1 bag of chicken feed
  - 3 lbs. of wildflower seed
- Grocery
  - milk
  - eggs
  - glass cleaner
  - isopropyl Alcohol
- Deli
  - rye bread
  - sesame rolls
  - 1 lbs. turkey sliced thin
- Tech Gear
    - cat 6e
      - 100 feet of cable
      - 50 rj-45 terminations
    - 2x 4k monitors (at least 32" each)
    - wireless mouse
    - wireless keyboard

### Text Editing

# h1 heading
## h2 heading
### h3 heading
#### h4 heading
... and so on

There are many options for editing text with Markdown. It is quite a bit faster than typing out HTML code.
> Still, practice, practice, practice.

If you want to make a *point* you have _several_ options for making *italics*

If you want to make a **heavier point** you can do that too.

Sometimes you want to tell people to touch the `Enter` key.

~~Markdown is difficult to understand.~~

<!--
Sometimes you have a work in progress
or want to remove some text, but not hit the "delete" key
-->

### Numbered Lists

To do when I wake up:

1. Shower
0. Brush teeth
0. Floss
0. Comb hair
0. Grab wallet
0. Put briefcase in car
0. Head to airport

Numbered lists can begin at any integer we would like:

17. Sweep driveway
0. Pull weeds in garden
0. Make coffee
0. Feed cat



```mermaid
sequenceDiagram
Alice ->> Proxy: SIP INVITE + SDP
Proxy -->> Alice: 100
Proxy --x Bob: SIP INVITE + SDP
Note right of Bob: This should not occur<br/>why the failure?
Proxy ->> Bob: SIP INVITE + SDP
Bob -->> Proxy: 100
Bob ->> Proxy: 180
Proxy ->> Alice: 180
Bob ->> Proxy: 200 + SDP
Proxy ->> Alice: 200 + SDP
Alice ->> Proxy: ACK
Proxy ->> Bob: ACK
Alice-->Bob: RTP
```


```mermaid
graph LR
A[Square Rect] -- Link text --> B((Circle))
A --> C(Round Rect)
B --> D{Rhombus}
C --> D
```
